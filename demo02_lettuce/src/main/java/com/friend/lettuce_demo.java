package com.friend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zyc
 * @since 2023-06-09 11:12
 */
@SpringBootApplication
public class lettuce_demo {
    public static void main(String[] args) {
        SpringApplication.run(lettuce_demo.class, args);
    }
}
