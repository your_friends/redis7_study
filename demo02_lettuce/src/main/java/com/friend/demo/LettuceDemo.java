package com.friend.demo;

import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.SortArgs;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author zyc
 * @since 2023-06-09 11:14
 */
@Slf4j
public class LettuceDemo {
    public static void main(String[] args) {
        // 使用构建器 RedisURI.builder
        RedisURI uri = RedisURI.builder().redis("192.168.10.233").withPort(6379).
                //withAuthentication("default","123456")// 连接windows版会失败
                        withPassword("123456")
                .build();
        // 创建连接客户端
        RedisClient client = RedisClient.create(uri);
        StatefulRedisConnection connect = client.connect();
        //操作命令api
        RedisCommands commands = connect.sync();

        // keys
        List<String> keys = commands.keys("*");
        keys.forEach(k -> log.info("========================" + k));

        // string
        commands.set("k1", "20230609am");
        String v1 = (String) commands.get("k1");
        System.out.println("========================value of k1  = " + v1);

        // list

        commands.rpush("list1", "轮播1", "轮播2", "轮播3");
        List list1 = commands.lrange("list1", 0, -1);
        list1.forEach(l -> System.out.println("value of list = " + l));


        // set
        commands.sadd("set1", "男", "girl");
        Set set1 = commands.smembers("set1");
        set1.forEach(s -> System.out.println("value of set = " + s));
        //hash
        HashMap<Object, Object> map = new HashMap<>();
        map.put("phone", "18595275016");
        map.put("name", "zyc");
        map.put("email", "2980428572@qq.com");
        commands.hmset("hash1", map);
        Map hash1 = commands.hgetall("hash1");
        hash1.forEach((k, v) -> System.out.println("hash1 k =" + k + ",v =" + v));

        //zset
        commands.zadd("zset1", 100.0, "商品1", 110.0, "商品2", 90.0, "商品3");
        List zset1 = commands.zrange("zset1", 0, 10);
        zset1.forEach(z -> System.out.println("zset z" + z));


        //sort
        SortArgs sortArgs = new SortArgs();
        sortArgs.alpha();
        sortArgs.desc();

        List list11 = commands.sort("list1", sortArgs);
        list11.forEach(ll -> System.out.println("sort list1 = " + ll));

        // 最后一步--关闭
        connect.close();
        client.shutdown();
    }
}
