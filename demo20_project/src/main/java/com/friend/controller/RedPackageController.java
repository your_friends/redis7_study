package com.friend.controller;

import cn.hutool.core.util.IdUtil;
import com.google.common.primitives.Ints;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * 发红包和抢红包的代码
 * @author zyc
 * @since 2023-06-15 14:30
 */
@Api(tags = "抢红包")
@RestController
public class RedPackageController {
    public static final String RED_PACKAGE_KEY = "redPackage";
    public static final String RED_PACKAGE_CONSUME_KEY = "redPackage:consume:";
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 发红包，生成红包的key和要求数量的红包金额数组
     * @param totalMoney 总金额
     * @param redPackageNumber 数量
     * @return key和 所有的每个红包金额
     */
    @ApiOperation("发")
    @GetMapping("/send")
    public String sendRedPackage(int totalMoney, int redPackageNumber) {
        //1 拆红包，将总金额totalMoney拆分为redPackageNumber个子红包
        Integer[] splitRedPackages = splitRedPackageAlgorithm(totalMoney, redPackageNumber);//拆分红包算法通过后获得的多个子红包数组
        //2 发红包并保存进list结构里面且设置过期时间
        String key = RED_PACKAGE_KEY + IdUtil.simpleUUID();
        redisTemplate.opsForList().leftPushAll(key, splitRedPackages);
        redisTemplate.expire(key, 1, TimeUnit.DAYS);
        //3 发红包OK，返回前台显示
        return key + "\t" + Ints.asList(Arrays.stream(splitRedPackages).mapToInt(Integer::valueOf).toArray());
    }

    /**
     * 抢红包
     * @param redPackageKey 此红包的key
     * @param userId 用户Id
     * @return 抢红包的结果
     */
    @ApiOperation("抢")
    @PostMapping("/rob")
    public String robRedPackage(String redPackageKey, String userId) {
        //1 验证某个用户是否抢过红包，不可以多抢
        Object redPackage = redisTemplate.opsForHash().get(RED_PACKAGE_CONSUME_KEY + redPackageKey, userId);
        //2 没有抢过可以去抢红包，否则返回-2表示该用户抢过红包了
        if (null == redPackage) {
            //2.1 从大红包(list)里面出队一个作为该客户抢的红包，抢到了一个红包
            Object partRedPackage = redisTemplate.opsForList().leftPop(RED_PACKAGE_KEY + redPackageKey);
            if (partRedPackage != null) {
                //2.2 抢到红包后需要记录进入hash结构，表示谁抢到了多少钱的某个子红包
                redisTemplate.opsForHash().put(RED_PACKAGE_CONSUME_KEY + redPackageKey, userId, partRedPackage);
                System.out.println("用户:" + userId + "\t 抢到了红包：" + partRedPackage);
                //TODO 后续异步进mysql或者MQ进一步做统计处理,每一年你发出多少红包，抢到了多少红包，年度总结
                return String.valueOf(partRedPackage);
            }
            // 抢完了
            return "errorCode:-1,红包抢完了";
        }
        //3 某个用户抢过了，不可以作弊抢多次
        return "errorCode:-2,   message:" + userId + "\t" + "你已经抢过红包了，不能重复抢";
    }

    @ApiOperation("查")
    @GetMapping("/search")
    public String searchRedPackage(String redPackageKey, String userId) {
        Object o = redisTemplate.opsForHash().get(RED_PACKAGE_CONSUME_KEY + redPackageKey, userId);
        return null == o ? "没有此人的抢红包记录" : o.toString();
    }

    /**
     * 拆红包的算法-->二倍均值算法
     *
     * @param totalMoney        总金额
     * @param redPackageNumber  数量
     * @return  要求数量的红包金额数组
     */
    private Integer[] splitRedPackageAlgorithm(int totalMoney, int redPackageNumber) {
        Integer[] redPackageNumbers = new Integer[redPackageNumber];
        //已经被抢夺的红包金额,已经被拆分塞进子红包的金额
        int useMoney = 0;
        for (int i = 0; i < redPackageNumber; i++) {
            if (i == redPackageNumber - 1) {
                redPackageNumbers[i] = totalMoney - useMoney;
            } else {
                //二倍均值算法，每次拆分后塞进子红包的金额 = 随机区间(0,(剩余红包金额M ÷ 未被抢的剩余红包个数N) * 2)
                int avgMoney = ((totalMoney - useMoney) / (redPackageNumber - i)) * 2;
                redPackageNumbers[i] = 1 + new Random().nextInt(avgMoney - 1);
            }
            useMoney = useMoney + redPackageNumbers[i];
        }
        return redPackageNumbers;
    }
}
