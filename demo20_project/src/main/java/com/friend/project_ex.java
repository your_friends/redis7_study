package com.friend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zyc
 * @since 2023-06-13 11:35
 */
@SpringBootApplication
public class project_ex {
    public static void main(String[] args) {
        SpringApplication.run(project_ex.class, args);
    }
}
