package com.friend.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @author zyc
 * @since 2023-06-13 11:34
 */
@Service
@Slf4j
public class HyperLogLogService {
    @Resource
    private RedisTemplate redisTemplate;

    /**
     * 模拟后台有用户点击淘宝首页，每个用户来自不同的IP地址
     * <p>
     * 项目启动后会执行此方法
     */
    @PostConstruct
    public void initIP() {
        new Thread(() -> {
            String ip = null;
            for (int i = 0; i < 200; i++) {
                Random random = new Random();
                ip = random.nextInt(256) + "." +
                        random.nextInt(256) + "." +
                        random.nextInt(256) + "." +
                        random.nextInt(256);
                // 返回1表示插入成功，是第一次访问；返回0表示已不是第一访问
                Long hll = redisTemplate.opsForHyperLogLog().add("hll", ip);
                log.info("ip={},该IP地址访问首页的次数={}", ip, hll);
                //暂停3秒钟
                try {
                    TimeUnit.SECONDS.sleep(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "t1").start();
    }

    public long uv() {
        // PFCOUNT key
        return redisTemplate.opsForHyperLogLog().size("hll");
    }

}
