## 介绍
`使用Geo、HyperLogLog、Bitmap解决实际问题及编写抢红包的逻辑`

## 步骤
1. GeoController
    >美团地图位置附近的酒店推送GEO
2. HyperLogLogController
   >淘宝亿级UV的Redis统计方案
3. Bitmap(案例不在此模块)
   > 可以用于缓存击穿充当布隆过滤器的功能
4. RedPackageController
   > 发红包、抢红包、查询红包