package com.friend.redlock.controller;

import lombok.extern.slf4j.Slf4j;
import org.redisson.RedissonMultiLock;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author zyc
 * @since 2023-06-13 12:10
 */
@RestController
@Slf4j
public class RedlockController {
    public static final String CACHE_KEY_REDLOCK = "FRIEND_REDLOCK";

    @Autowired
    RedissonClient redissonClient1;
    @Autowired
    RedissonClient redissonClient2;
    @Autowired
    RedissonClient redissonClient3;

    @GetMapping("/multiLock")
    public String getMultiLock() {
        String taskThreadID = Thread.currentThread().getId() + "";
        RLock lock1 = redissonClient1.getLock(CACHE_KEY_REDLOCK);
        RLock lock2 = redissonClient2.getLock(CACHE_KEY_REDLOCK);
        RLock lock3 = redissonClient3.getLock(CACHE_KEY_REDLOCK);

        RedissonMultiLock redLock = new RedissonMultiLock(lock1, lock2, lock3);
        redLock.lock();

        try {
            log.info("come in biz multiLock:", taskThreadID);
            try {
                TimeUnit.SECONDS.sleep(30);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            log.info("task is over multiLock:", taskThreadID);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("multiLock exception:", e.getCause() + "\t" + e.getMessage());
        } finally {
            redLock.unlock();
            log.info("释放分布式锁成功key:", CACHE_KEY_REDLOCK);
        }
        return "multiLock task is over : " + taskThreadID;
    }
}
