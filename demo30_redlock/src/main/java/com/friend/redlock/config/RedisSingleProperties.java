package com.friend.redlock.config;

import lombok.Data;

/**
 * @author zyc
 * @since 2023-06-13 12:10
 */
@Data
public class RedisSingleProperties {
    private  String address1;
    private  String address2;
    private  String address3;
}