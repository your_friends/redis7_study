package com.friend.redlock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zyc
 * @since 2023-06-13 12:09
 */
@SpringBootApplication
public class RedlockApplication {
    public static void main(String[] args) {
        SpringApplication.run(RedlockApplication.class,args);
    }
}
