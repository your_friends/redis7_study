package com.friend.canal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zyc
 * @since 2023-06-13 10:22
 */
//@SpringBootApplication
public class CanalDemo {
    public static void main(String[] args) {
        SpringApplication.run(CanalDemo.class,args);
    }
}
