## 介绍
`使用canal实现缓存数据双写问题`

## 步骤
1. 设置mysql
    - 修改配置文件
    - 重启服务
    - 新建用户
2. 下载canal并配置和mysql的连接
   - 下载解压
   - 修改配置文件
   - 启动服务
   - 查看日志文件
3. 使用本demo测试效果
    - 配置redis的信息
    - 配置mysql的信息
    - 配置canal的信息
    - 启动类RedisCanalClientExample中主程序