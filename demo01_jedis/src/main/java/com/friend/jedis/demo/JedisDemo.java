package com.friend.jedis.demo;

import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;

import java.util.List;
import java.util.Set;

/**
 * @author zyc
 * @since 2023-06-09 10:43
 */
@Slf4j
public class JedisDemo {
    public static void main(String[] args) throws InterruptedException {
        // 通过指定ip和端口号获得connection
        Jedis jedis = new Jedis("192.168.10.233", 6379);
        // 指定访问服务器的密码
        jedis.auth("123456");
        // 获得jedis客户端，可以像jdbc一样，访问redis
        System.out.println("jedis.ping() = " + jedis.ping());
        // keys
        Set<String> keys = jedis.keys("*");
        keys.forEach(k -> System.out.println("keys  K " + k));

        // string
        jedis.set("k1", "20230609");
        log.info("k1 value:{}" + jedis.get("k1"));
        System.out.println(jedis.ttl("k1"));
        jedis.expire("k1", 20L);
        Thread.sleep(2000L);
        System.out.println(jedis.ttl("k1"));
        // list
        jedis.rpush("list", "11", "22", "33");
        List<String> list = jedis.lrange("list", 0, -1);
        list.forEach(l -> System.out.println("list l = " + l));

        jedis.flushDB();
        jedis.close();

    }
}
