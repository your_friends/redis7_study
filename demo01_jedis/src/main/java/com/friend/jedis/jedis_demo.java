package com.friend.jedis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zyc
 * @since 2023-06-09 10:40
 */
@SpringBootApplication
public class jedis_demo {
    public static void main(String[] args) {
        SpringApplication.run(jedis_demo.class,args);
    }
}
