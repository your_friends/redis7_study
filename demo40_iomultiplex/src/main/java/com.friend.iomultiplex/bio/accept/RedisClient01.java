package com.friend.iomultiplex.bio.accept;


import java.io.IOException;
import java.net.Socket;

/**
 * @author zyc
 * @since 2023-06-15 10:22
 */
public class RedisClient01
{
    public static void main(String[] args) throws IOException
    {
        System.out.println("------RedisClient01 start");
        Socket socket = new Socket("127.0.0.1", 6379);
        System.out.println("------RedisClient01 connection over");
    }
}
