package com.friend.redislock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @auther zyc
 * @create 2023-06-20 15:58
 */
@SpringBootApplication
public class RedisDistributedLockApp8888
{
    public static void main(String[] args)
    {
        SpringApplication.run(RedisDistributedLockApp8888.class,args);
    }
}
