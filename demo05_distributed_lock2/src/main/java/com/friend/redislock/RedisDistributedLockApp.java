package com.friend.redislock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zyc
 * @since 2023-06-13 15:28
 */
@SpringBootApplication
public class RedisDistributedLockApp {
    public static void main(String[] args) {
        SpringApplication.run(RedisDistributedLockApp.class, args);
    }
}
