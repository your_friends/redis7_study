package com.friend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zyc
 * @since 2023-06-09 14:35
 */
@SpringBootApplication
public class template_demo {
    public static void main(String[] args) {
        SpringApplication.run(template_demo.class, args);
    }
}
