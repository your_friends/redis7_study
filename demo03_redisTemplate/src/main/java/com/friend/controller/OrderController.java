package com.friend.controller;

import com.friend.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author zyc
 * @since 2023-06-09 14:51
 */
@Api(tags = "订单接口")
@RestController
@Slf4j
public class OrderController {
    @Resource
    private OrderService orderService;

    @ApiOperation("新增订单")
    @PostMapping("/order/add")
    public void addOrder() {
        orderService.addOrder();
    }

    @ApiOperation("按orderId查订单信息")
    @GetMapping("/order/{id}")
    public String findUserById(@PathVariable Integer id) {
        return orderService.getOrderById(id);
    }
}
