package com.friend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zyc
 * @since 2023-06-20 17:28
 */
@SpringBootApplication
public class JhsApplication {
    public static void main(String[] args) {
        SpringApplication.run(JhsApplication.class, args);
    }
}
