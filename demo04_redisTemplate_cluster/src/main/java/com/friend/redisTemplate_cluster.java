package com.friend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zyc
 * @since 2023-06-09 16:04
 */
@SpringBootApplication
public class redisTemplate_cluster {
    public static void main(String[] args) {
        SpringApplication.run(redisTemplate_cluster.class, args);
    }
}
