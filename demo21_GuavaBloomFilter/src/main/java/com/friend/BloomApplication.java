package com.friend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author zyc
 * @since 2023-06-20 17:29
 */
@SpringBootApplication
@MapperScan("com.friend.mapper") //import tk.mybatis.spring.annotation.MapperScan;
public class BloomApplication {
    public static void main(String[] args) {
        SpringApplication.run(BloomApplication.class, args);
    }
}
