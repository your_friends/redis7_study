package com.friend.service;

import com.friend.entities.Customer;
import com.friend.mapper.CustomerMapper;
import com.friend.utils.CheckUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @author zyc
 * @since 2023-06-20 17:06
 */
@Service
@Slf4j
public class CustomerSerivce {
    public static final String CACHE_KEY_CUSTOMER = "customer:";

    @Resource
    private CustomerMapper customerMapper;
    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private CheckUtils checkUtils;

    /**
     * 写操作
     *
     * @param customer
     */
    public void addCustomer(Customer customer) {
        int i = customerMapper.insertSelective(customer);

        if (i > 0) {
            //mysql插入成功，需要重新查询一次将数据捞出来，写进redis
            Customer result = customerMapper.selectByPrimaryKey(customer.getId());

            //redis缓存key
            String key = CACHE_KEY_CUSTOMER + customer.getId();
            //捞出来的数据写进redis
            redisTemplate.opsForValue().set(key, result);

            // 新插入的数据放入白名单
            int hashValue = Math.abs(key.hashCode());
            long index = (long) (hashValue % Math.pow(2, 32));
            log.info(key + " 对应的坑位index:{}", index);
            redisTemplate.opsForValue().setBit("whitelistCustomer", index, true);
        }
    }

    public Customer findCustomerById(Integer customreId) {
        Customer customer = null;
        //缓存redis的key名称
        String key = CACHE_KEY_CUSTOMER + customreId;
        //1 先去redis查询
        customer = (Customer) redisTemplate.opsForValue().get(key);

        //2 redis有直接返回，没有再进去查询mysql
        if (customer == null) {
            // 3 再去查询我们的mysql
            customer = customerMapper.selectByPrimaryKey(customreId);
            // 3.1 mysql有，redis无
            if (customer != null) {
                //3.2 把mysq查询出来的数据回写redis，保持一致性
                redisTemplate.opsForValue().set(key, customer);
            }
        }
        return customer;
    }

    /**
     * 加强补充，避免突然key失效了，打爆mysql，做一下预防，尽量不出现击穿的情况。
     *
     * @param customreId
     * @return
     */
    public Customer findCustomerById2(Integer customreId) {
        Customer customer = null;
        //缓存redis的key名称
        String key = CACHE_KEY_CUSTOMER + customreId;
        //1 先去redis查询
        // 第1次查询redis，加锁前
        customer = (Customer) redisTemplate.opsForValue().get(key);
        //2 redis有直接返回，没有再进去查询mysql
        if (customer == null) {
            //2 大厂用，对于高QPS的优化，进来就先加锁，保证一个请求操作，让外面的redis等待一下，避免击穿mysql
            synchronized (CustomerSerivce.class) {
                //第2次查询redis，加锁后
                customer = (Customer) redisTemplate.opsForValue().get(key);
                //3 二次查redis还是null，可以去查mysql了(mysql默认有数据)
                if (customer == null) {
                    //4 查询mysql拿数据(mysql默认有数据)
                    customer = customerMapper.selectByPrimaryKey(customreId);
                    if (customer == null) {
                        return null;
                    } else {
                        //5 mysql里面有数据的，需要回写redis，完成数据一致性的同步工作
                        redisTemplate.opsForValue().setIfAbsent(key, customer, 7L, TimeUnit.DAYS);
                    }
                }
            }

        }
        return customer;
    }


    /**
     * BloomFilter → redis → mysql
     * 白名单：whitelistCustomer
     *
     * @param customerId
     * @return
     */
    public Customer findCustomerByIdWithBloomFilter(Integer customerId) {
        Customer customer = null;
        //缓存key的名称
        String key = CACHE_KEY_CUSTOMER + customerId;

        //布隆过滤器check，无是绝对无，有是可能有
        //===============================================
        if (!checkUtils.checkWithBloomFilter("whitelistCustomer", key)) {
            log.info("白名单无此顾客，不可以访问: " + key);
            return null;
        }
        //===============================================

        //1 查询redis
        customer = (Customer) redisTemplate.opsForValue().get(key);
        //redis无，进一步查询mysql
        if (customer == null) {
            //2 从mysql查出来customer
            customer = customerMapper.selectByPrimaryKey(customerId);
            // mysql有，redis无
            if (customer != null) {
                //3 把mysql捞到的数据写入redis，方便下次查询能redis命中。
                redisTemplate.opsForValue().set(key, customer);
            }
        }
        return customer;
    }
}
